import time
import os.path
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select, WebDriverWait
import pickle

contact = "Anku"
text = "Hi Diego, this is an automated testing message."

options = webdriver.ChromeOptions()
options.add_argument(r'user-data-dir=./wpp_data')

driver = webdriver.Chrome(chrome_options=options)

driver.get("https://web.whatsapp.com/")
time.sleep(15)

inp_xpath_search = '//*[@id="side"]/div[1]/div/label/div/div[2]'
input_box_search = WebDriverWait(driver, 50).until(
    lambda driver: driver.find_element_by_xpath(inp_xpath_search))
input_box_search.click()
time.sleep(2)
input_box_search.send_keys(contact)
time.sleep(2)
selected_contact = driver.find_element_by_xpath("//span[@title='"+contact+"']")
selected_contact.click()
inp_xpath = '//*[@id="main"]/footer/div[1]/div[2]/div/div[2]'
input_box = driver.find_element_by_xpath(inp_xpath)
time.sleep(2)
input_box.send_keys(text + Keys.ENTER)
time.sleep(2)
driver.quit()
