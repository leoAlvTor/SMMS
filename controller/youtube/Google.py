import pickle
import os
from google_auth_oauthlib.flow import Flow, InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.http import MediaFileUpload, MediaIoBaseDownload
from google.auth.transport.requests import Request


def crear_conexion(client_secret, api, api_version, *scopes):
    scopes = [scopes for scopes in scopes[0]]
    credencial = None

    archivo_picke = f'token_{api}_{api_version}.pickle'

    if os.path.exists(archivo_picke):
        with open(archivo_picke, 'rb') as token:
            credencial = pickle.load(token)

    if not credencial or not credencial.valid:
        if credencial and credencial.expired and credencial.refresh_token:
            credencial.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(client_secret, scopes)

    try:
        service = build(api, api_version, credentials=credencial)
        return service
    except Exception as e:
        print('ERROR AL CONECTAR CON LA API')
        return None
