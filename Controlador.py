import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from time import gmtime, strftime


class Controlador:
    SCOPES = ['https://www.googleapis.com/auth/spreadsheets']
    SPREADSHEET_ID = '1D9WXh5gCf9YVWUjr2VRTF3gwnbXrHP8tw3Rs-8ZrSx0'
    HOJA_MENSAJES = 'MENSAJES!A2:C'
    HOJA_PARAMETROS = 'PARAMETROS!A1:H'
    INDEX_ESTADO = 0
    INDEX_HORA = 0
    SERVICE = None

    def leer_datos(self):
        credentials = self.get_token()
        self.SERVICE = build('sheets', 'v4', credentials=credentials)

        sheet = self.SERVICE.spreadsheets()
        mensajes = sheet.values().get(spreadsheetId=self.SPREADSHEET_ID, range=self.HOJA_MENSAJES).execute()
        parametros = sheet.values().get(spreadsheetId=self.SPREADSHEET_ID, range=self.HOJA_PARAMETROS).execute()
        valuesMensajes = mensajes.get('values', [])
        valuesParametros = parametros.get('values', [])

        if not valuesMensajes and not valuesParametros:
            print('No se ha encontrado ningun dato en la hojas seleccionadas.')
        else:
            print('MENSAJE \t HASHTAGS \t LINK DE IMAGEN')
            for row in valuesMensajes:
                print(row[0], '\t', row[1], '\t', row[2])
            matriz_datos = []
            for row in valuesParametros:
                for i in range(len(row)):
                    if row[i] == 'ESTADO':
                        self.INDEX_ESTADO = i
                    elif row[i] == 'HORA ENVIO':
                        self.INDEX_HORA = i
                matriz_datos.append(self.actualizar_fila(row))
            self.actualizar_hoja(matriz_datos, parametros)

    def get_token(self):
        credentials = None
        if os.path.exists('token.pickle'):
            with open('token.pickle', 'rb') as token:
                credentials = pickle.load(token)
        if not credentials or not credentials.valid:
            if credentials and credentials.expired and credentials.refresh_token:
                credentials.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'client_secret.json', self.SCOPES)
                credentials = flow.run_local_server(port=0)
            with open('token.pickle', 'wb') as token:
                pickle.dump(credentials, token)
        return credentials

    def actualizar_fila(self, fila):
        if fila[0] != 'FACEBOOK':
            fila_actualizada = []
            for i in range(0, len(fila)):
                if i == self.INDEX_ESTADO:
                    cell_value = 'ENVIADO'
                elif i == self.INDEX_HORA:
                    cell_value = strftime("%Y-%m-%d %H:%M:%S", gmtime())
                else:
                    cell_value = fila[i]
                fila_actualizada.append(cell_value)
            print(fila_actualizada)
            return fila_actualizada
        else:
            return fila

    def actualizar_hoja(self, values, hoja):
        range_ = self.HOJA_PARAMETROS
        value_input_option = 'RAW'
        value_range_body={
            'values':values
        }
        request = self.SERVICE.spreadsheets().values().update(spreadsheetId=self.SPREADSHEET_ID, range=range_, valueInputOption=value_input_option, body=value_range_body)
        response = request.execute()
        print(response)
