from Controlador import Controlador


def main():
    controlador = Controlador()
    controlador.leer_datos()


if __name__ == '__main__':
    main()